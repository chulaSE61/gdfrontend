import React, { Component } from 'react';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import HomePage from './scenes/HomePage'
import LoginPage from './scenes/LoginPage'

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={HomePage}/>
            <Route exact path="/login" component={LoginPage}/>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
